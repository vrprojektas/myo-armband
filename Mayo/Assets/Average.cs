﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Average : MonoBehaviour
{
	public ThalmicMyo myo;
	public TextMeshProUGUI avg;
	public TextMeshProUGUI maxAvg;

	// Update is called once per frame
	void Update()
    {
		avg.text = myo.avg.ToString();
		maxAvg.text = myo.maxavg.ToString();
    }
}
