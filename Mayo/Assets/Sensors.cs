﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Sensors : MonoBehaviour
{
	public ThalmicMyo myo;
    public TextMeshProUGUI[] sensor;

	// Update is called once per frame
	void Update()
	{
        for (int i = 0; i < myo.saveData.Length; i++)
        {
            sensor[i].text = myo.saveData[i].ToString();
        }
	}
}
