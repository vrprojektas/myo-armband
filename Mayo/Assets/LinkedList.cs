﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedList
{
        Node pr;
        Node pb;
        Node current;
        public int lenght;

        public LinkedList()
        {
            lenght = 1;
            pr = null;
            pb = null;
            current = null;
        }

        public void AddToList(int Data)
        {
            var dd = new Node(Data, pb, null);
            if (pr != null)
                pb.Right = dd;
            else
                pr = dd;
            pb = dd;

            lenght++;
        }

        public void first()
        {
            current = pr;
        }

        public void last()
        {
            current = pb;
        }

        public bool IsEmpty()
        {
            return current != null;
        }
        public float returnData()
        {
            return current.Data;
        }

        public Node ReturnCurrent()
        {
            return current;
        }

        public void remove(int a)
        {
            if (pr.Data == a)
            {
                pr = pr.Right;
            }
            for (Node g = pr; g != null; g = g.Right)
            {
                if (g.Data == a)
                {
                    if (g.Right != null)
                        g.Right.Left = g.Left;
                    if (g.Left != null)
                        g.Left.Right = g.Right;

                }
            }
            if (pb.Data == a)
            {
                pb = pb.Left;
            }
        }


        private void InsertMidle(Node iterpiamasis) // kai reikia paduoti mazga
        {
            int a = 0;
            for (Node d = pr; d != null; d = d.Right)
            {
                if (a == 1) // pavizdys sugalvotas
                {
                    iterpiamasis.Right = d.Right;
                    iterpiamasis.Left = d;
                    d.Right.Left = iterpiamasis;
                    d.Right = iterpiamasis;
                }
                a++;
            }
        }

        private void insertAfterElement(Node rr, int kk)
        {
            Node dd = new Node(kk, rr, rr.Right);
            if (pb == rr)
                pb = dd; // Įterpti pabaigoje (už paskutinio)
            else
                dd.Right.Left = dd;
            rr.Right = dd;
        }

        private void insertBeforeElement(Node rr, int kk)
        {
            if (rr == pr)
            { // Įterpti prieš pirmąjį
                Node dd = new Node(kk, null, pr);
                pr.Left = dd;
                pr = dd;
            }
        }

        public Node FindNode(int Data) // grazina mazga
        {
            for (Node d = pr; d != null; d = d.Right)
            {
                if (d.Data == Data)
                {
                    return d;
                }

            }
            return null;
        }

        public float FindData(int Data) // grazina duomenis
        {
            for (Node d = pr; d != null; d = d.Right)
            {
                if (d.Data == Data)
                {
                    return d.Data;
                }

            }
            return 0;
        }

        public void ToRight()
        {

            current = current.Right;
        }

        public void ToLeft()
        {

            current = current.Left;
        }
    
}

public class Node
{
    public float Data { get; set; }
    public Node Left;
    public Node Right;

    public Node(float data, Node addressL, Node addressR)
    {
        Data = data;
        Left = addressL;
        Right = addressR;
    }
}
