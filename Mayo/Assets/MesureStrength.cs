﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MesureStrength : MonoBehaviour
{
    [SerializeField] ThalmicMyo strength;
    [SerializeField] float maximum;
    [SerializeField] float maxY = 0;
    float locationY = 0;
    [SerializeField] float value = 0;
    [SerializeField] float tempValue = 0;
    [SerializeField] float time;
    [SerializeField] int seperateParts;
    float timeCount = 0;
    float increase = 0;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
            value = strength.maxavg * 100 / maximum;
            locationY = maxY * 0.01f * value;
            increase = locationY / seperateParts;

		if (timeCount < time)
		{
			timeCount += (Time.deltaTime * 10);
		}
		else if (timeCount > time && tempValue < locationY)
		{
			timeCount = 0;
			tempValue += increase;
			transform.localPosition = new Vector2(transform.localPosition.x, tempValue);
		}
	}
}
